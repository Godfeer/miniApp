package com.example.miniapp.entitiy;


import javax.persistence.*;
import java.util.UUID;


@Entity
public class Information {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;
    private String uuId;
    /***
     * 商家名称
     */
    private String companyName;
    /***
     * 商家头像
     */
    private String businessesFace;
    /***
     * 商家电话
     */
    private String businessPhone;
    /***
     * 简介
     */
    private String synopsis;

    public Information() {
    }

    public Information(String companyName, String businessesFace, String businessPhone, String synopsis) {
        this.uuId = UUID.randomUUID().toString()+"";
        this.companyName = companyName;
        this.businessesFace = businessesFace;
        this.businessPhone = businessPhone;
        this.synopsis = synopsis;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUuId() {
        return uuId;
    }

    public void setUuId(String uuid) {
        this.uuId = uuid;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getBusinessesFace() {
        return businessesFace;
    }

    public void setBusinessesFace(String businessesFace) {
        this.businessesFace = businessesFace;
    }

    public String getBusinessPhone() {
        return businessPhone;
    }

    public void setBusinessPhone(String businessPhone) {
        this.businessPhone = businessPhone;
    }

    public String getSynopsis() {
        return synopsis;
    }

    public void setSynopsis(String synopsis) {
        this.synopsis = synopsis;
    }
}

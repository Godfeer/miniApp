package com.example.miniapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

@SpringBootApplication(scanBasePackages = {"com"})
public class MiniappApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(MiniappApplication.class, args);
	}
	/**
	 *重写 configure
	 * @param builder
	 * @return
	 */
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		return builder.sources(MiniappApplication.class);
	}
}

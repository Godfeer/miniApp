package com.example.miniapp.repository;

import com.example.miniapp.entitiy.Information;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


public interface InfoRepository extends JpaRepository<Information, Long> {

    /***
     * 按名称查
     * @param name
     * @return
     */
    Information findByCompanyName(String name);


}

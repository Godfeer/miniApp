package com.example.miniapp.util;

public interface Content {
     String LOGINERROR = "-1";
     String LoginPass = "用户名或密码错误，请重试！";

     String LoginSuccess = "验证成功！";
     String FilledWithNo = "待填写！";
     String LogError = "Error";
     String LogSuccess = "Success";

     String SignError = "签名错误！";

     String Sign = "76a224230d60b04a4384333479ff2fe65b3c42278cf61d2a";
}

package com.example.miniapp.util.vo;


import com.jfinal.kit.JsonKit;

/**
 * 功能描述: 封装ajax传入
 * @author godfery
 * email: gaoxinobject@gmail.com
 * date: 2018年8月23日 下午9:50:58
 */

public class AjaxAfferent {
    // 传入的中文消息
    private String cipher;

    // 传入时携带的数据
    private Object data;


    public String getCipher() {
        return cipher;
    }

    public void setCipher(String cipher) {
        this.cipher = cipher;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return JsonKit.toJson(this);
    }
}

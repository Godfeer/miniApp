package com.example.miniapp.util.rsa.rsa1;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RSAController {
    private static final String DEFAULT_PRIVATE_KEY=
            "MIICeAIBADANBgkqhkiG9w0BAQEFAASCAmIwggJeAgEAAoGBAKpLfAX+D3RSESgJ\n" +
                    "4idmgT5qSkB4O/jUGz3OIa2nmQj1B351XYN7eLEUaqkNL+GZdOTiG1iCAxDRQVuK\n" +
                    "arume2S7s2zD42bJfQJ+Nj94LYyFGNWZNQ7rPUbUYkVowEEruO6Mk/EjeOSk5H8F\n" +
                    "RZMNxU6iP6ezafUUeeiRP5WfaHNjAgMBAAECgYBUkbv7tdmy6HaArbfT3UHd3ycU\n" +
                    "sSEHRz5oCRAMjyienCCmml5gR1b+iacWYOggXvcAG5F8+MPQ/Do63Zh3rAumgVsO\n" +
                    "UIsDr5fC+9UhMq6v5V0H/6qFxG3FXmcHMliK6Q1VcONPCkwAjaDbogALkqbMBkzu\n" +
                    "MQpoK+mRb3dJIgU4sQJBANetD9DxvF0hlBqTyupqlTSpOHlMwFaZ0jCdk5THywIW\n" +
                    "WgkhJNwdJ5p9F4MU9Lhk2AOqKaxm+H4wC2VzOsCHdmUCQQDKIlYf6P7ttOBeIK1o\n" +
                    "vv9GI+cWy3WujVKofBSwr8mbKwAhp7AnXiuO0j5I+giEJNXZjdvNtmlaSlNGkVdS\n" +
                    "HCInAkEA1569YTdMCg+38ZowE4va/3ruatbAs9O0v8XJ9cSIwN13xgjMwQKkxV6q\n" +
                    "UvJwdqjbDU7huup3JDEEM7iv1CuZwQJBAKB6WEQnWFXil4tDiEkz8jl+gZZwW/ME\n" +
                    "Ek7fjBGmznnZztXpawomUpCmTHrlv2MiV4HMhWix3Lypa6oH8/qliVsCQQCBceQU\n" +
                    "CjtmXOY/obqMthngb2t2xE1Rh+OsuffDcTkCciKOf1FF2gYjLNfrkwYe2ryExgFo\n" +
                    "kAtikXdP/KCOIpFV";
    private static final String DEFAULT_PUBLIC_KEY=
            "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCqS3wF/g90UhEoCeInZoE+akpAeDv41Bs9ziGtp5kI9Qd+dV2De3ixFGqpDS/hmXTk4htYggMQ0UFbimq7pntku7Nsw+NmyX0CfjY/eC2MhRjVmTUO6z1G1GJFaMBBK7jujJPxI3jkpOR/BUWTDcVOoj+ns2n1FHnokT+Vn2hzYwIDAQAB";

    /***
     * 签名生成接口  a 内容
     */
    @RequestMapping(value = "RSASignature")
    private String RSASignature(@RequestParam String a ) throws Exception {

        //私钥加密
        byte[] cipherData= RSAEncrypt.encrypt(RSAEncrypt.loadPrivateKeyByStr(DEFAULT_PRIVATE_KEY),a.getBytes());
        String cipher= Base64.encode(cipherData);

        //签名
        String signstr= RSASignature.sign(a,DEFAULT_PRIVATE_KEY);
        return signstr+":"+cipher;
    }
    /***
     * 签名验证接口  a 内容  b  秘钥
     */
    @RequestMapping(value = "RSASign")
    private String RSASign(@RequestParam String a, @RequestParam String b ) throws Exception {
//        int index = cis.indexOf(":");
//        String signstr = cis.substring(index);
//        String right = cis.substring(index+cis.length()+1);
        byte[] ress=RSAEncrypt.decrypt(RSAEncrypt.loadPublicKeyByStr(DEFAULT_PUBLIC_KEY), Base64.decode(b));
        String restr=new String(ress);

        return restr+ RSASignature.doCheck(restr, a, DEFAULT_PUBLIC_KEY);
    }




}

package com.example.miniapp.util.rsa.rsa1;

public class MainTest {
    private static final String DEFAULT_PUBLIC_KEY=
            "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCqS3wF/g90UhEoCeInZoE+akpAeDv41Bs9ziGtp5kI9Qd+dV2De3ixFGqpDS/hmXTk4htYggMQ0UFbimq7pntku7Nsw+NmyX0CfjY/eC2MhRjVmTUO6z1G1GJFaMBBK7jujJPxI3jkpOR/BUWTDcVOoj+ns2n1FHnokT+Vn2hzYwIDAQAB";

    private static final String DEFAULT_PRIVATE_KEY=
            "MIICeAIBADANBgkqhkiG9w0BAQEFAASCAmIwggJeAgEAAoGBAKpLfAX+D3RSESgJ\n" +
                    "4idmgT5qSkB4O/jUGz3OIa2nmQj1B351XYN7eLEUaqkNL+GZdOTiG1iCAxDRQVuK\n" +
                    "arume2S7s2zD42bJfQJ+Nj94LYyFGNWZNQ7rPUbUYkVowEEruO6Mk/EjeOSk5H8F\n" +
                    "RZMNxU6iP6ezafUUeeiRP5WfaHNjAgMBAAECgYBUkbv7tdmy6HaArbfT3UHd3ycU\n" +
                    "sSEHRz5oCRAMjyienCCmml5gR1b+iacWYOggXvcAG5F8+MPQ/Do63Zh3rAumgVsO\n" +
                    "UIsDr5fC+9UhMq6v5V0H/6qFxG3FXmcHMliK6Q1VcONPCkwAjaDbogALkqbMBkzu\n" +
                    "MQpoK+mRb3dJIgU4sQJBANetD9DxvF0hlBqTyupqlTSpOHlMwFaZ0jCdk5THywIW\n" +
                    "WgkhJNwdJ5p9F4MU9Lhk2AOqKaxm+H4wC2VzOsCHdmUCQQDKIlYf6P7ttOBeIK1o\n" +
                    "vv9GI+cWy3WujVKofBSwr8mbKwAhp7AnXiuO0j5I+giEJNXZjdvNtmlaSlNGkVdS\n" +
                    "HCInAkEA1569YTdMCg+38ZowE4va/3ruatbAs9O0v8XJ9cSIwN13xgjMwQKkxV6q\n" +
                    "UvJwdqjbDU7huup3JDEEM7iv1CuZwQJBAKB6WEQnWFXil4tDiEkz8jl+gZZwW/ME\n" +
                    "Ek7fjBGmznnZztXpawomUpCmTHrlv2MiV4HMhWix3Lypa6oH8/qliVsCQQCBceQU\n" +
                    "CjtmXOY/obqMthngb2t2xE1Rh+OsuffDcTkCciKOf1FF2gYjLNfrkwYe2ryExgFo\n" +
                    "kAtikXdP/KCOIpFV";




    public static void main(String[] args) throws Exception {
        String filepath="G:/tmp/";


        //RSAEncrypt.genKeyPair(filepath);

        System.out.println("--------------公钥加密私钥解密过程-------------------");
        String plainText="ihep_公钥加密私钥解密";
        //公钥加密过程
        byte[] cipherData=RSAEncrypt.encrypt(RSAEncrypt.loadPublicKeyByStr(DEFAULT_PUBLIC_KEY),plainText.getBytes());
        String cipher=Base64.encode(cipherData);
        //私钥解密过程
        byte[] res=RSAEncrypt.decrypt(RSAEncrypt.loadPrivateKeyByStr(DEFAULT_PRIVATE_KEY), Base64.decode(cipher));
        String restr=new String(res);
        System.out.println("原文："+plainText);
        System.out.println("加密："+cipher);
        System.out.println("解密："+restr);
        System.out.println();

        System.out.println("--------------私钥加密公钥解密过程-------------------");
        plainText="ihep_私钥加密公钥解密";
        //私钥加密过程
        cipherData=RSAEncrypt.encrypt(RSAEncrypt.loadPrivateKeyByStr(DEFAULT_PRIVATE_KEY),plainText.getBytes());
        cipher=Base64.encode(cipherData);
        //公钥解密过程
        res=RSAEncrypt.decrypt(RSAEncrypt.loadPublicKeyByStr(DEFAULT_PUBLIC_KEY), Base64.decode(cipher));
        restr=new String(res);
        System.out.println("原文："+plainText);
        System.out.println("加密："+cipher);
        System.out.println("解密："+restr);
        System.out.println();

        System.out.println("---------------私钥签名过程------------------");
        String content="ihep_这是用于签名的原始数据";
        String signstr= RSASignature.sign(content,DEFAULT_PRIVATE_KEY);
        System.out.println("签名原串："+content);
        System.out.println("签名串："+signstr);
        System.out.println();

        System.out.println("---------------公钥校验签名------------------");
        System.out.println("签名原串："+content);
        System.out.println("签名串："+signstr);
        System.out.println("验签结果："+RSASignature.doCheck(content, signstr, DEFAULT_PUBLIC_KEY));
        System.out.println();



    }
}


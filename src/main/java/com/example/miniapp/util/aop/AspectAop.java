package com.example.miniapp.util.aop;


import com.jfinal.kit.JsonKit;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;

//申明是个切面
@Aspect
//申明是个spring管理的bean
@Component
public class AspectAop {
    //使用 org.slf4j.Logger , 这是 Spring 实现日志的方法
    private final static Logger logger = LoggerFactory.getLogger(AspectAop.class);


    ThreadLocal<Long> startTime = new ThreadLocal<Long>();
    //申明一个切点 里面是 execution表达式
    @Pointcut("execution(public * com.example.miniapp..*.*(..))")
    private void log() {
    }
    //请求method前打印内容
    @Before(value = "log()")
    public void methodBefore(JoinPoint joinPoint) {

        startTime.set(System.currentTimeMillis());

        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        //javax.servlet.http.HttpServletRequest  这个包下
        HttpServletRequest request = attributes.getRequest();
        //打印请求内容
        logger.info("===============请求内容start===============");
        //URL
        logger.info("请求地址:" + request.getRequestURL().toString());
        //method
        logger.info("请求方式:" + request.getMethod());
        //ip
        logger.info("ip = {}",request.getRemoteAddr());
        //类  getDeclaringTypeName  类名  getSignature.getName  类方法
        logger.info("请求类方法:" + joinPoint.getSignature().getDeclaringTypeName() +"." + joinPoint.getSignature().getName());
        logger.info("请求类方法参数:" + Arrays.toString(joinPoint.getArgs()));
        logger.info("===============请求内容end===============");

    }

    @After(value = "log()")
    private void methodAfter(){

    }

    //在方法执行完结后打印返回内容
    @AfterReturning(returning = "object", pointcut = "log()")
    public void methodAfterReturing(Object object) {
        logger.info("--------------返回内容start----------------");
        logger.info("Response内容:" + JsonKit.toJson(object));
        logger.info("--------------返回内容end----------------");
        logger.info("请求处理时间为:"+(System.currentTimeMillis() - startTime.get()));
    }


}

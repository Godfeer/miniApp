package com.example.miniapp.controller;

import com.example.miniapp.entitiy.Information;
import com.example.miniapp.repository.InfoRepository;
import com.example.miniapp.util.vo.AjaxAfferent;
import com.example.miniapp.util.vo.AjaxResult;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

import static com.example.miniapp.util.Content.LogSuccess;
import static com.example.miniapp.util.Content.Sign;
import static com.example.miniapp.util.Content.SignError;

@RestController
public class InfoController {

    @Autowired
    InfoRepository informationRepository;
    private AjaxResult ajax = new AjaxResult();
    private List<Information> information;
    private JSONArray jsonArray;
    private JSONObject jsonObject;


    @RequestMapping(value = "InfoInstall")
    @ResponseBody
    @Transactional()
    public AjaxResult InfoInstall(@RequestBody AjaxAfferent afferent){
        if ( Sign.equals(afferent.getCipher())){
            information = new ArrayList<Information>();

            jsonArray = JSONArray.fromObject(afferent.getData());

            for (int i = 0; i < jsonArray.size(); i++) {
                jsonObject = jsonArray.getJSONObject(i);
                information.add((Information) JSONObject.toBean(jsonObject, Information.class));
            }
            for (Information in:information) {
                informationRepository.save(in);
            }

//            for (int i = 0; i <information.size() ; i++) {
//
//                System.out.println("#################"+information.get(i));
//                informationRepository.save(information.get(i));
//            }

            ajax.success(information.size());

        }else {
            ajax.addError(SignError);
        }

          System.out.println(afferent.getData());

        return ajax;
    }

    @RequestMapping(value = "InfoCompanyName")
    @ResponseBody
    public AjaxResult InfoCompanyName(@RequestBody AjaxAfferent afferent){
        if ( Sign.equals(afferent.getCipher())){

            String data = afferent.getData().toString();
            ajax.success(informationRepository.findByCompanyName(data));

        }else {
            ajax.addError(SignError);
        }
        return ajax;
    }

    @RequestMapping(value = "InfoSelect")
    @ResponseBody
    public AjaxResult InfoSelect(){
        ajax.setMessage(LogSuccess);
        ajax.success(informationRepository.findAll());
        return ajax;
    }
}
